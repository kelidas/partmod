/* 
 * File:   dmgalpha.h
 * Author: Robson
 *
 * Created on 16 kwiecień 2012, 11:39
 */

#ifndef DMGALPHA_H
#define	DMGALPHA_H

#include <exceptions/exceptions.hpp>
#include <geometry/geometry.hpp>
#include <model/model.hpp>
#include <utils/utils.hpp>
#include <container/container.hpp>
#include <trajectory/trajectory.hpp>
#include <diagram/diagram.hpp>
#include <diagram/kinetic_diagram.hpp>
#include <shape/shape.hpp>
#include <graph/computation_graph.hpp>


#endif	/* DMGALPHA_H */

