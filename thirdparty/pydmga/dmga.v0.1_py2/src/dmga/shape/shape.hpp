/*
 * shape.hpp
 *
 *  Created on: 12-11-2012
 *      Author: Robson
 */

#ifndef SHAPE_HPP_
#define SHAPE_HPP_

#include <shape/shape.h>

#include <shape/alpha_shape.hpp>
#include <shape/sasa_shape.hpp>


#endif /* SHAPE_HPP_ */
