# Open Academic Solver
Open Academic Solver (OAS) is a project developing numerical solver primarily focused on mechanical behavior of solids. It also solves mass transport, heat conducion or other scalar problems described by Poisson's equation. Large effort is devoted to multiphysical coupled tasks. Both discrete and continuous material representations are included.

## Documentation
https://kelidas.gitlab.io/OAS-site

## Executables
- executables for Windows and "Ubuntu 22.04"
http://t7610pc1.stm.fce.vutbr.cz:9080/OAS/


