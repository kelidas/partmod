The purpose of this benchmark is test of single particle contact loaded in shear using periodic functions

Explanation of loading functions is in "functions.inp"

to obtain graphs of loads and displacements run "plot_LD_single_spring.py" after solution
