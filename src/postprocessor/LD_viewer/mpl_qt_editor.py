#!/usr/bin/python
# -*- coding: utf-8 -*-
# Pierre Haessig — March 2014
"""
Qt adaptation of Gael Varoquaux's tutorial to integrate Matplotlib
http://docs.enthought.com/traitsui/tutorials/traits_ui_scientific_app.html#extending-traitsui-adding-a-matplotlib-figure-to-our-application
based on Qt-based code shared by Didrik Pinte, May 2012
http://markmail.org/message/z3hnoqruk56g2bje
adapted and tested to work with PySide from Anaconda in March 2014
Last Modification: Modified by Brendan Griffen 2019 (github: bgriffen).
This works with the current syntax required for backend_qt4agg.
"""

from pyface.qt import QtGui, QtCore
#
import matplotlib as mpl
#mpl.use('Qt4Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT

from traits.api import Any, Instance
from traitsui.qt.editor import Editor
from traitsui.basic_editor_factory import BasicEditorFactory
from traitsui.api import Handler

class _MPLFigureEditor(Editor):

    scrollable = True

    def init(self, parent):
        self.control = self._create_canvas(parent)
        self.object.on_trait_change(self.update_editor, 'data_changed')
        self.value.canvas.mpl_connect('key_press_event', self._key_press_callback)
        self.set_tooltip()

    def update_editor(self):
        figure = self.value
        figure.canvas.draw()

    def _create_canvas(self, parent):
        """ Create the MPL canvas. """
        # matplotlib commands to create a canvas
        frame = QtGui.QWidget()
        mpl_canvas = FigureCanvas(self.value)
        mpl_canvas.setParent(frame)
        mpl_toolbar = NavigationToolbar2QT(mpl_canvas, frame)

        mpl_canvas.setFocusPolicy(QtCore.Qt.FocusPolicy.ClickFocus)
        mpl_canvas.setFocus()

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(mpl_canvas)
        vbox.addWidget(mpl_toolbar)
        frame.setLayout(vbox)

        return frame

    def _key_press_callback(self, event):
        'whenever a key is pressed'
        figure = self.value
        if not event.inaxes:
            return
        if event.key == 'l':
            if figure.axes[0].get_xscale() == 'log':
                figure.axes[0].set_xscale('linear')
                figure.canvas.draw()
            else:
                figure.axes[0].set_xscale('log')
                figure.canvas.draw()

        if event.key == 'k':
            if figure.axes[0].get_yscale() == 'log':
                figure.axes[0].set_yscale('linear')
                figure.canvas.draw()
            else:
                figure.axes[0].set_yscale('log')
                figure.canvas.draw()

class MPLFigureEditor(BasicEditorFactory):

    klass = _MPLFigureEditor

class MPLInitHandler(Handler):
    """Handler calls mpl_setup() to initialize mpl events"""

    def init(self, info):
        """This method gets called after the controls have all been
        created but before they are displayed.
        """
        info.object.mpl_setup()
        return True

if __name__ == "__main__":
    # Create a window to demo the editor
    from traits.api import HasTraits
    from traitsui.api import View, Item
    from numpy import sin, cos, linspace, pi
    from matplotlib.widgets import RectangleSelector

    class Test(HasTraits):

        figure = Instance(Figure, ())

        view = View(Item('figure', editor=MPLFigureEditor(),
                         show_label=False),
                    handler=MPLInitHandler,
                    resizable=True)

        def __init__(self):
            super(Test, self).__init__()
            self.axes = self.figure.add_subplot(111)
            t = linspace(0, 2*pi, 200)
            self.axes.plot(sin(t)*(1+0.5*cos(11*t)), cos(t)*(1+0.5*cos(11*t)))

        def mpl_setup(self):
            def onselect(eclick, erelease):
                print("eclick: {}, erelease: {}".format(eclick,erelease))

            self.rs = RectangleSelector(self.axes, onselect,useblit=True)

    Test().configure_traits()
